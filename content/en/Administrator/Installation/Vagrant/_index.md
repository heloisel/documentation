+++
title = "Vagrant"
date = 2020-07-22T19:06:46+02:00
weight = 10
+++


You may have Asqatasun installed seamlessly with the Vagrant installation.

See dedicated repository [Asqatasun-Docker](https://gitlab.com/asqatasun/asqatasun-vagrant/).
