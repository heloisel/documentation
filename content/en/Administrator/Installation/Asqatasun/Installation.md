---
title: "Installation"
date: 2020-07-22T19:07:19+02:00
draft: false
weight: 40
---


You should have already done these steps:

1. [Check Hardware provisioning]({{< relref "./Hardware_network_provisioning" >}})
1. [Check pre-requisites]({{< relref "./Pre-requisites" >}})

(Ever need help ? Go to [Asqatasun Forum](https://forum.asqatasun.org).)

## Download Asqatasun binaries

Retrieve the [lastest version of Asqatasun](https://download.asqatasun.org/asqatasun-latest.tar.bz2) and extract it on your file system. 

```shell script
mkdir -p /tmp/asqatasun
cd /tmp/asqatasun
wget https://download.asqatasun.org/asqatasun-latest.tar.bz2
tar xfj asqatasun-latest.tar.bz2
```

Place binaries in `/opt/asqatasun`:

```shell script
mkdir -p /opt/asqatasun
cp server/asqatasun-server/target/asqatasun-server-5.0.0-rc.1.jar /opt/asqatasun/
cp web-app/asqatasun-web-app/target/asqatasun-web-app-5.0.0-rc.1.war /opt/asqatasun/
```

Create symbolic links to binaries:

```shell script
ASQA_RELEASE="5.0.0-rc.1"
APP_FILE="asqatasun-web-app-${ASQA_RELEASE}.war"
API_FILE="asqatasun-server-${ASQA_RELEASE}.jar"

cd "/opt/asqatasun/"
ln -s "${APP_FILE}" "asqatasun-webapp.war"
ln -s "${API_FILE}" "asqatasun-rest-server.jar"
```

## Create user and directories

```shell script
ASQA_SYSTEM_USER="asqatasun"
ASQA_HOME="/home/${ASQA_SYSTEM_USER}"

useradd -c "Asqatasun user" -d "${ASQA_HOME}" -l -s /bin/bash "${ASQA_SYSTEM_USER}"
mkdir -p /var/log/asqatasun/
chown -R asqatasun /var/log/asqatasun/
mkdir -p "${ASQA_HOME}/.dconf"
chown -R asqatasun "${ASQA_HOME}" 
```

## Systemd unit: .env file

Create Systemd units for Asqatasun web application and Asqatasun API

```shell script
DB_DRIVER="mysql"
DB_HOST="localhost"
DB_PORT="3306"
DB_DATABASE="asqatasun"
DB_USER="asqatasunDatabaseUserLogin"
DB_PASSWORD="asqatasunDatabaseUserP4ssword"

cat >/etc/systemd/system/asqatasun-db.service.env <<EOF
JDBC_URL=jdbc:${DB_DRIVER}://${DB_HOST}:${DB_PORT}/${DB_DATABASE}
JDBC_USER=${DB_USER}
JDBC_PASSWORD=${DB_PASSWORD}
EOF
chmod 600 /etc/systemd/system/asqatasun-db.service.env
```

## Systemd unit: Asqatasun web application

```shell script
APP_LOG_DIR="/var/log/asqatasun/webapp"
APP_PORT="8080"

cat >/etc/systemd/system/asqatasun-webapp.service <<EOF
[Unit]
Description=asqatasun-webapp
After=syslog.target

[Service]
User=asqatasun
EnvironmentFile=/etc/systemd/system/asqatasun-db.service.env
ExecStart=/usr/bin/java -Dwebdriver.firefox.bin=/opt/firefox/firefox -Dwebdriver.gecko.driver=/opt/geckodriver -Dapp.logging.path=${APP_LOG_DIR} -jar asqatasun-webapp.war --spring.profiles.active=webapp --server.port=${APP_PORT}
SuccessExitStatus=143
Restart=always
WorkingDirectory=/opt/asqatasun

[Install]
WantedBy=multi-user.target
EOF
```

## Systemd unit: Asqatasun API 

```shell script
API_PORT="8081"
API_LOG_DIR="/var/log/asqatasun/api"

cat >/etc/systemd/system/asqatasun-api.service <<EOF
[Unit]
Description=asqatasun-rest-server
After=syslog.target

[Service]
User=asqatasun
EnvironmentFile=/etc/systemd/system/asqatasun-db.service.env
ExecStart=/usr/bin/java -Dwebdriver.firefox.bin=/opt/firefox/firefox -Dwebdriver.gecko.driver=/opt/geckodriver -Dapp.logging.path=${API_LOG_DIR} -Dapp.engine.loader.selenium.headless=true -jar asqatasun-rest-server.jar --spring.profiles.active=webapp --server.port=${API_PORT} --management.server.port=8091
SuccessExitStatus=143
Restart=always
WorkingDirectory=/opt/asqatasun

[Install]
WantedBy=multi-user.target
EOF
```

## Start Asqatasun 

```shell script
systemctl start asqatasun-webapp
systemctl start asqatasun-api
```

## Get status of Asqatasun services

```shell script
systemctl status asqatasun-webapp
systemctl status asqatasun-api
```

## Browse Asqatasun logs

```shell script
journalctl -u asqatasun-webapp.service
journalctl -u asqatasun-api.service
```

`-f` option continuously prints new entries as they are appended to the journal

## Stop Asqatasun 

```shell script
systemctl stop asqatasun-webapp
systemctl stop asqatasun-api
```

## Start & stop manually Asqatasun **WEB APPLICATION** 

If you want more fine grained control over startup, you can run directly the binaries.

```shell script
APP_PORT="8080"
DB_PORT="3306"
DB_HOST="localhost"
DB_DRIVER="mysql"
DB_DATABASE="asqatasun"
DB_USER="asqatasunDatabaseUserLogin"
DB_PASSWORD="asqatasunDatabaseUserP4ssword"
APP_LOG_DIR="/var/log/asqatasun/webapp"

/usr/bin/java \
    -Dwebdriver.firefox.bin=/opt/firefox/firefox  \
    -Dwebdriver.gecko.driver=/opt/geckodriver     \
    -Dapp.logging.path="${APP_LOG_DIR}"           \
    -Djdbc.url="jdbc:${DB_DRIVER}://${DB_HOST}:${DB_PORT}/${DB_DATABASE}" \
    -Djdbc.user="${DB_USER}" \
    -Djdbc.password="${DB_PASSWORD}" \
    -jar /opt/asqatasun/asqatasun-webapp.war \
    --server.port="${APP_PORT}"
```

To stop Asqatasun web application, just kill the process.

## Start & stop manually Asqatasun **API**

If you want more fine grained control over startup, you can run directly the binaries.

```shell script
API_PORT="8081"
DB_PORT="3306"
DB_HOST="localhost"
DB_DRIVER="mysql"
DB_DATABASE="asqatasun"
DB_USER="asqatasunDatabaseUserLogin"
DB_PASSWORD="asqatasunDatabaseUserP4ssword"
API_LOG_DIR="/var/log/asqatasun/api"

/usr/bin/java \
    -Dwebdriver.firefox.bin=/opt/firefox/firefox  \
    -Dwebdriver.gecko.driver=/opt/geckodriver  \
    -Dapp.logging.path="${API_LOG_DIR}" \
    -Dapp.engine.loader.selenium.headless=true \
    -Djdbc.url="jdbc:${DB_DRIVER}://${DB_HOST}:${DB_PORT}/${DB_DATABASE}" \
    -Djdbc.user="${DB_USER}" \
    -Djdbc.password="${DB_PASSWORD}" \
    -jar /opt/asqatasun/asqatasun-rest-server.jar \
    --server.port="${API_PORT}" \
    --management.server.port=8091
```

To stop Asqatasun web application, just kill the process.

<!---
## Next step

You can go to "Add an Apache frontend".
-->
