+++
title = "Regular Installation"
date = 2020-07-22T19:06:46+02:00
weight = 5
+++


This page describes the steps to follow to install Asqatasun 
(and further) from the binary files or from the sources. The software has been 
tested on Linux Ubuntu 18.04 LTS (Bionic Beaver).

## Steps to install (recommended)

1. [Check Hardware and network provisioning]({{< relref "./Hardware_network_provisioning.md" >}})
1. [Check pre-requisites]({{< relref "./Pre-requisites.md" >}})
1. [Install]({{< relref "./Installation.md" >}})

<!---
1. Set up an Apache frontend with HTTPS (optional)
1. Add web analytics (Piwik, Matomo...) (optional)
-->

Ever need help ? Go to [Asqatasun Forum](http://forum.asqatasun.org).
