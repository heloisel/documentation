---
title: "Adding Web analytics"
date: 2020-07-22T19:07:19+02:00
draft: true
weight: 60
---


You should have already done these steps:

1. [Check Hardware provisioning]({{< relref "./Hardware_network_provisioning" >}})
1. [Check pre-requisites]({{< relref "./Pre-requisites" >}})
1. [Install]({{< relref "./Installation" >}})

## Configurins web analytics (Piwik, Matomo...)

You may add the analytics code in the file `/var/lib/tomcat8/webapps/asqatasun/WEB-INF/view/template/tracker.jsp`

Example of `tracker.jsp` with Matomo

```jsp
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<!-- Matomo -->
<script type="text/javascript">
    var _paq = window._paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
        var u="//analytics.asqatasun.example.org/";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '1']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
    })();
</script>
<noscript><p><img src="//analytics.asqatasun.example.org/matomo.php?idsite=10&amp;rec=1" style="border:0;" alt="" /></p></noscript>
<!-- End Matomo Code -->
```
