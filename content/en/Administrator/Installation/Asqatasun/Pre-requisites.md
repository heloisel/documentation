---
title: "Pre-requisites"
date: 2020-07-22T19:07:19+02:00
draft: false
weight: 30
---


You should have already done these steps:

1. [Check Hardware provisioning]({{< relref "./Hardware_network_provisioning" >}})

## Note

This page describes in a human fashion the pre-requisites for Asqatasun. If you want code that automates stuff 
with no further explanation, leverage our [`bootstrap.sh`](https://gitlab.com/asqatasun/asqatasun-vagrant/-/blob/master/5.x/5.0.y/5.0.0-rc.1/Asqatasun.5.0.0-rc.1_mysql.5.7_ubuntu.18.04_jdk8/bootstrap.sh)
from [Asqatasun-Vagrant](https://gitlab.com/asqatasun/asqatasun-vagrant/).

## Supported OSes (yet)

* Ubuntu 18.04

## Required packages

* Java JRE 8
* Mysql 5.7
* libGTK 3
* libDbus

```shell script
apt-get --no-install-recommends install \
    openjdk-8-jre          \
    libgtk-3-0             \
    libdbus-glib-1-2       \
    mysql-server
```

Note: MariaDB support is in progress (eee [issue #504](https://gitlab.com/asqatasun/Asqatasun/-/issues/504)), any help is warmly welcome!

## Database

Create a database and the associated user:

```shell script
DB_DATABASE="asqatasun"
DB_USER="asqatasunDatabaseUserLogin"
DB_PASSWORD="asqatasunDatabaseUserP4ssword"

echo "GRANT ALL PRIVILEGES ON *.* TO '${DB_USER}'@'%' IDENTIFIED BY '${DB_PASSWORD}';" >> db.sql
echo "CREATE DATABASE ${DB_DATABASE};" >> db.sql
mysql -u root < db.sql
rm db.sql
service mysql restart
```

## Firefox ESR

* Firefox ESR 78
* Gecko Driver

```shell script
FIREFOX_VERSION="78.2.0esr"
GECKODRIVER_VERSION="v0.26.0"
FIREFOX_URL_PREFIX="https://download-installer.cdn.mozilla.net/pub/firefox/releases/"
GECKODRIVER_URL_PREFIX="https://github.com/mozilla/geckodriver/releases/download/"
FIREFOX_URL="${FIREFOX_URL_PREFIX}${FIREFOX_VERSION}/linux-x86_64/en-US/firefox-${FIREFOX_VERSION}.tar.bz2"
GECKODRIVER_URL="${GECKODRIVER_URL_PREFIX}${GECKODRIVER_VERSION}/geckodriver-${GECKODRIVER_VERSION}-linux64.tar.gz"

cd /opt
sudo wget "${FIREFOX_URL}"
sudo wget "${GECKODRIVER_URL}"
sudo tar xf "firefox-${FIREFOX_VERSION}.tar.bz2"
sudo tar xf "geckodriver-${GECKODRIVER_VERSION}-linux64.tar.gz"
```

Please note that Firefox version *does* matter: if you use something different from 78 ESR we cannot guaranty anything. 

## Mail SMTP

Asqatasun works better with email (informing you when an audit is finished, or if gives an error).
Here are the steps to install locally an SMTP server. You can also use online services 
such as MailJet or Mandrill and configure it into `/etc/asqatasun/asqatasun.org`

Install the following packages:

```shell script
sudo apt-get install postfix mailutils
sudo dpkg-reconfigure postfix
```
Once the configuration is displayed, options to take are :

* **Configuration type:** Satellite system
* **SMTP relay:** *leave it empty* (this is the trick, don't type anything here)

## Next step

Congratulations, you survived the manual pre-requisites stage :)

You can go to [Installation]({{< relref "./Installation" >}}).
