---
title: "Scenario Audit"
date: 2020-11-20T19:50:28+01:00
---

<!---
*** WARNING *** WARNING *** WARNING *** WARNING *** WARNING *** WARNING ***

     This page is linked from within the Asqatasun app 
     in /web-app/asqatasun-web-app/src/main/resources/i18n/scenario-management-I18N(..)

     Do not rename, move or delete please :)
     
*** WARNING *** WARNING *** WARNING *** WARNING *** WARNING *** WARNING ***
-->

## Pre-requisites

Client side:

* [Firefox](https://www.mozilla.org/firefox/new/) 68+
* Extension [Selenium IDE](https://addons.mozilla.org/fr/firefox/addon/selenium-ide/)

Server side:

* Asqatasun v5.0.0-rc.1 at least

## Use cases

* Audit a multi-step form
* Audit a single page application in various states
* Audit a buying funnel on an e-commerce website
* Audit a business web application 

## How to create a scenario

TODO

