+++
title = "RGAA 3.0 Theme 8: Mandatory elements"
date = 2020-11-15T18:18:18+02:00
weight = 10
+++

## Criterion 8.1
Is each Web page defined by a document type?
* [Rule 8.1.1]({{< relref "./Rule-8-1-1" >}})
* [Rule 8.1.2]({{< relref "./Rule-8-1-2" >}})
* [Rule 8.1.3]({{< relref "./Rule-8-1-3" >}})

## Criterion 8.2
For each Web page, is the source code valid according
to the specified document type (except in particular cases)?
* [Rule 8.2.1]({{< relref "./Rule-8-2-1" >}})
* [Rule 8.2.2]({{< relref "./Rule-8-2-2" >}})

## Criterion 8.3
On each Web page, is the default human language identifiable?
* [Rule 8.3.1]({{< relref "./Rule-8-3-1" >}})

## Criterion 8.4
For each Web page with a default human language,
is the language code appropriate?
* [Rule 8.4.1]({{< relref "./Rule-8-4-1" >}})

## Criterion 8.5
Does each Web page have a page title?
* [Rule 8.5.1]({{< relref "./Rule-8-5-1" >}})

## Criterion 8.6
 For each Web page with a page title, is this title relevant?
* [Rule 8.6.1]({{< relref "./Rule-8-6-1" >}})

## Criterion 8.7
On each Web page, is each change in the human language
identified via the source code (except in particular cases)?
* [Rule 8.7.1]({{< relref "./Rule-8-7-1" >}})

## Criterion 8.8
On each Web page, is each change in human language relevant?
* [Rule 8.8.1]({{< relref "./Rule-8-8-1" >}})
* [Rule 8.8.2]({{< relref "./Rule-8-8-2" >}})

## Criterion 8.9
On each Web page, tags must not be used only for layout.
Has this rule been followed?
* [Rule 8.9.1]({{< relref "./Rule-8-9-1" >}})

## Criterion 8.10
On each Web page, are changes in reading direction identified?
* [Rule 8.10.1]({{< relref "./Rule-8-10-1" >}})


