+++
title = "RGAA 3.0 Theme 3: Colours"
date = 2020-11-15T18:18:18+02:00
weight = 10
+++

## Criterion 3.1
On each Web page, information must not be conveyed only through color.
Has this rule been followed?
* [Rule 3.1.1]({{< relref "./Rule-3-1-1" >}})
* [Rule 3.1.2]({{< relref "./Rule-3-1-2" >}})
* [Rule 3.1.3]({{< relref "./Rule-3-1-3" >}})
* [Rule 3.1.4]({{< relref "./Rule-3-1-4" >}})
* [Rule 3.1.5]({{< relref "./Rule-3-1-5" >}})
* [Rule 3.1.6]({{< relref "./Rule-3-1-6" >}})

## Criterion 3.2
On each Web page, information  must not be conveyed only through color.
Has this rule been implemented in a relevant way?
* [Rule 3.2.1]({{< relref "./Rule-3-2-1" >}})
* [Rule 3.2.2]({{< relref "./Rule-3-2-2" >}})
* [Rule 3.2.3]({{< relref "./Rule-3-2-3" >}})
* [Rule 3.2.4]({{< relref "./Rule-3-2-4" >}})
* [Rule 3.2.5]({{< relref "./Rule-3-2-5" >}})
* [Rule 3.2.6]({{< relref "./Rule-3-2-6" >}})

## Criterion 3.3
On each Web page, is the contrast between the text
and background colors sufficient (except in particular cases)?
* [Rule 3.3.1]({{< relref "./Rule-3-3-1" >}})
* [Rule 3.3.2]({{< relref "./Rule-3-3-2" >}})
* [Rule 3.3.3]({{< relref "./Rule-3-3-3" >}})
* [Rule 3.3.4]({{< relref "./Rule-3-3-4" >}})

## Criterion 3.4
On each Web page, is the contrast between the text
and background colors enhanced (except in particular cases)?
* [Rule 3.4.1]({{< relref "./Rule-3-4-1" >}})
* [Rule 3.4.2]({{< relref "./Rule-3-4-2" >}})
* [Rule 3.4.3]({{< relref "./Rule-3-4-3" >}})
* [Rule 3.4.4]({{< relref "./Rule-3-4-4" >}})

