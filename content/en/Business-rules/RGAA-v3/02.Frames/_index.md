+++
title = "RGAA 3.0 Theme 2: Frames"
date = 2020-11-15T18:18:18+02:00
weight = 10
+++

## Criterion 2.1
 Does each iframe have a frame title?
* [Rule 2.1.1]({{< relref "./Rule-2-1-1" >}})

## Criterion 2.2
For each iframe with a frame title, is this frame title relevant?
* [Rule 2.2.1]({{< relref "./Rule-2-2-1" >}})

