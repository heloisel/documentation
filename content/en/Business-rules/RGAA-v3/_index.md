+++
title = "RGAA v3"
date = 2020-11-22T17:14:53+02:00
weight = 60
+++

## RGAAv3 figures

RGAA 3.0 is compound of 13 themes:

* [Theme 1 - Images]({{< relref "./01.Images/" >}})
* [Theme 2 - Frames]({{< relref "./02.Frames/" >}})
* [Theme 3 - Colors]({{< relref "./03.Colours" >}})
* [Theme 4 - Multimedia]({{< relref "./04.Multimedia" >}})
* [Theme 5 - Tables]({{< relref "./05.Tables" >}})
* [Theme 6 - Links]({{< relref "./06.Links" >}})
* [Theme 7 - Script]({{< relref "./07.Scripts" >}})
* [Theme 8 - Mandatory elements]({{< relref "./08.Mandatory_elements" >}})
* [Theme 9 - Structure of information]({{< relref "./09.Structure_of_information" >}})
* [Theme 10 - Presentation of information]({{< relref "./10.Presentation_of_information" >}})
* [Theme 11 - Forms]({{< relref "./11.Forms" >}})
* [Theme 12 - Navigation]({{< relref "./12.Navigation" >}})
* [Theme 13 - Consultation]({{< relref "./13.Consultation" >}})

see also: [RGAA 3.0 - Rules specification](https://references.modernisation.gouv.fr/referentiel-technique-0)

For our own needs, we also created 
a [diff file between AccessiWeb 2.2 and RGAA 3.0](diff_RGAA-3_AccessiWeb-2.2_AccessiWeb-HTML5-ARIA.ods), LibreOffice Calc format.
