+++
title = "Theme 7: Scripts"
date = 2020-11-15T18:18:18+02:00
weight = 10
+++

## Criterion 7.1

> Chaque [script](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#script) est-il, si nécessaire, [compatible avec les technologies d’assistance](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#compatible-avec-les-technologies-d-assistance) ?

* [Rule 7.1.1]({{< relref "./Rule-7-1-1" >}})
* [Rule 7.1.2]({{< relref "./Rule-7-1-2" >}})
* [Rule 7.1.3]({{< relref "./Rule-7-1-3" >}})

## Criterion 7.2

> Pour chaque [script](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#script) ayant une alternative, cette alternative est-elle pertinente ?

* [Rule 7.2.1]({{< relref "./Rule-7-2-1" >}})
* [Rule 7.2.2]({{< relref "./Rule-7-2-2" >}})

## Criterion 7.3

> Chaque [script](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#script) est-il [contrôlable par le clavier et par tout dispositif de pointage](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#accessible-et-activable-par-le-clavier-et-tout-dispositif-de-pointage) (hors cas particuliers) ?

* [Rule 7.3.1]({{< relref "./Rule-7-3-1" >}})
* [Rule 7.3.2]({{< relref "./Rule-7-3-2" >}})

## Criterion 7.4

> Pour chaque [script](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#script) qui initie un [changement de contexte](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#changement-de-contexte), l’utilisateur est-il averti ou en a-t-il le contrôle ?

* [Rule 7.4.1]({{< relref "./Rule-7-4-1" >}})

## Criterion 7.5

> Dans chaque page web, les [messages de statut](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#message-de-statut) sont-ils correctement restitués par les technologies d’assistance ?

* [Rule 7.5.1]({{< relref "./Rule-7-5-1" >}})
* [Rule 7.5.2]({{< relref "./Rule-7-5-2" >}})
* [Rule 7.5.3]({{< relref "./Rule-7-5-3" >}})

