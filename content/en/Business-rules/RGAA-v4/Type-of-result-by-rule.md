---
title: "Type of Result by Rule"
date: 2020-11-12T17:33:10+01:00
---

## Introduction

We call a *rule* the implementation of a test.
For instance **Rule RGAAv4 5.1.1** is the implementation of [test RGAAv4 5.1.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#test-5-1-1) 
 
In Asqatasun, a rule may produce up to [5 kinds of results](User_manual/Kind-of-result.md). 
In the audit parameters, the user may provide [markers](User_manual/markers.md) to enlarge the scope and accuracy of the audit.
Depending on the audited web page, and the presence of markers, a rule can produce up to 4 kinds of result.

Here we list in an extensive way, which one(s) of the results a given rule can produce.

## Summary 

As of Asqatasun 5.0.0:

* **23** rules can produce *Passed*
* **42** rules can produce *Failed*
* **100** rules can produce *Not Applicable*
* **95** rules can produce *Pre-qualified*

We call a rule *decidable* when, in whatever circumstance, the produced results are included in Passed or Failed or Not Applicable.

* **17** rules are *decidable*

## Spreadsheet

Download the [spreadsheet "Type of result by Rule" (OpenDocument format, 23kb)]({{< baseurl >}}/files/Type-of-result-by-rule.ods)

## How to gather these data

From the directory holding all design sheets of RGAAv4:

```shell script
rgrep -i "#### Passed" */Rule-*
rgrep -i "#### Failed" */Rule-*
rgrep -i "#### Not App" */Rule-*
rgrep -i -EH "#### Pre|#### NMI" */Rule-*
rgrep -i "#### Not Tested" */Rule-*
```

