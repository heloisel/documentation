+++
title = "Rule 11.11.1"
date = 2020-11-15T18:18:18+02:00
weight = 10
+++

## Summary

This test consists in detecting `<form>` tags on the page.

The control that checks that input errors are displayed with expected types and formats has to be done manually.

## Business description

### Criterion

[11.11](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#crit-11-11)

### Test

[11.11.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#test-11-11-1)

### Description

> Pour chaque erreur de saisie, les types et les formats de données sont-ils suggérés, si nécessaire ?

#### Technical notes (criterion 11.11)

> Certains types de contrôles en HTML5 proposent des messages d’aide à la saisie automatique : par exemple le type email affiche un message du type « veuillez saisir une adresse e-mail valide » dans le cas où l’adresse e-mail saisie ne correspond pas au format attendu. Ces messages sont personnalisables via l’API Constraint Validation, ce qui permet de personnaliser les messages d’erreur et de valider le critère. L’attribut pattern permet d’effectuer automatiquement des contrôles de format (via des expressions régulières) et affiche un message d’aide personnalisable via l’attribut title : ce dispositif valide également le critère.
> 
> Référence : <a href="https://www.w3.org/TR/html52/sec-forms.html#the-constraint-validation-api">HTML 5.2 - 4.10.20.3 The constraint validation API</a>.

### Level

**AA**


## Technical description

### Scope

**Page**

### Decision level

**Semi-Decidable**

## Algorithm

### Selection

#### Set1

All the `<form>` tags. 

css selector :
```css
form
```

### Process

#### Test1

The selection handles the process.

For each occurence of the **Set1** raise a MessageA

##### MessageA: Manual check on element

- code: ManualCheckOnElements
- status: Pre-Qualified
- parameter: snippet
- present in source: yes

### Analysis

#### Not Applicable

The page has no `<form>` tag (**Set1** is empty)

#### Pre-qualified

In all other cases


## Notes

We detect the elements of the scope of the test 
to determine whether the test is applicable.

## Files

- [TestCases files for rule 11.11.1](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.0/src/test/resources/testcases/rgaa40/Rgaa40Rule111101/)
- [Unit test file for rule 11.11.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.0/src/test/java/org/asqatasun/rules/rgaa40/Rgaa40Rule111101Test.java)
- [Class file for rule 11.11.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.0/src/main/java/org/asqatasun/rules/rgaa40/Rgaa40Rule111101.java)
