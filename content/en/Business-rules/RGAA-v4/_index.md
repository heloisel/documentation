+++
title = "RGAA v4"
date = 2020-11-12T17:14:53+02:00
weight = 40
+++

## RGAAv4 figures

As of Asqatasun v5.0.0, the implementation of RGAA v4 covers:

* 117 rules (over 257 tests in RGAAv4)
* Please have a look at [Type of Result by Rule]({{< relref Type-of-result-by-rule >}}) for detailed figures

RGAA 4.0 is compound of 13 themes:
* [Theme 1 - Images]({{< relref "./01.Images/" >}})
* [Theme 2 - Frames]({{< relref "./02.Frames/" >}})
* [Theme 3 - Colors]({{< relref "./03.Colours" >}})
* [Theme 4 - Multimedia]({{< relref "./04.Multimedia" >}})
* [Theme 5 - Tables]({{< relref "./05.Tables" >}})
* [Theme 6 - Links]({{< relref "./06.Links" >}})
* [Theme 7 - Script]({{< relref "./07.Scripts" >}})
* [Theme 8 - Mandatory elements]({{< relref "./08.Mandatory_elements" >}})
* [Theme 9 - Structure of information]({{< relref "./09.Structure_of_information" >}})
* [Theme 10 - Presentation of information]({{< relref "./10.Presentation_of_information" >}})
* [Theme 11 - Forms]({{< relref "./11.Forms" >}})
* [Theme 12 - Navigation]({{< relref "./12.Navigation" >}})
* [Theme 13 - Consultation]({{< relref "./13.Consultation" >}})

see also: [RGAA 4.0 - Rules specification](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/)
