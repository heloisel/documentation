---
title: "Release Definition of Done"
date: 2020-11-12T10:05:11+01:00
weight: 10
---

Here is the *Definition of done*. This is meant to be copy/pasted in an issue to follow the progression of the release process.
Example [Issue #517 Producing Asqatasun release 5.0.0-beta.1 --> checklist](https://gitlab.com/asqatasun/Asqatasun/-/issues/517) 

## Software 

* [ ] CI is green
* [ ] Content of the archive is:
    * jar + war
    * documentation (zip or link to site)
* [ ] A krashtest campaign (smoke test) is OK
* [ ] An audit ran on 3 random web pages is checked by a human and all results are correct

## Docker

* [ ] [Docker repos](https://gitlab.com/asqatasun/asqatasun-docker) and its documentation are up-to-date
* [ ] Docker images are functional and published on Docker Hub

## Documentation

* [ ] [Documentation repos](https://gitlab.com/asqatasun/documentation) is up-to-date, especially install doc and upgrade doc.
* [ ] Content of the [main README](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/README.md) is curated (and kept simple)
* [ ] CHANGELOG is up-to-date
* [ ] Release Note for the final version is written
* [ ] List of results per test (passed, failed, NMI, NA, Not Tested) is up-to-date

## Communication

* [ ] A message is posted on the Forum to announce the release + explain new features (get inspired by what Gitlab Inc does)
* [ ] Publish announce on Twitter + retweet
