---
title: "Producing a Release"
date: 2020-11-12T10:34:32+01:00
weight: 20
---

Here are described the steps to produce a release

## 0. Create the *release checklist* issue

Copy / paste the [Definition of Done](Release-definition-of-done.md) into an issue.

## 0 Assumptions

* `${RELEASE_VERSION}` is the version string we're about to release. It must be SemVer compatible. Example: 5.0.0-alpha.2
* `${DEV_VERSION}` is the version string of the code base *after* the release is created.
  It is not considered as a version in the sense of SemVer. Example: 5.0.0-SNAPSHOT

```shell script
RELEASE_VERSION="5.0.0-alpha.2"
DEV_VERSION="5-SNAPSHOT"
```

## X. Update main `README.md`
## X. Update main `CHANGELOG.md`
## X. Update `contributors.txt`

From the root of the repos:

```shell script
git checkout master
engine/asqatasun-resources/src/main/resources/release/update_contributors.sh --to-version ${RELEASE_VERSION} 
git add contributors.txt
```

**Note**: the `update_contributors.sh` script relies on a `.mailmap` file, that groups various names / emails of 
a given contributor into a unique one (couple name/email). As the `.mailmap` file contains a lot of personal emails
we chose deliberately *not* to place it in the repository. 

## X. Release version

We work on branch `master` (this will be enhanced later; all leads are stored in [issue #494 + comments](https://gitlab.com/asqatasun/Asqatasun/-/issues/494)).

```shell script
engine/asqatasun-resources/src/main/resources/release/bump_asqatasun_v5.sh \
    --release-version "${RELEASE_VERSION}" \
    --dev-version "${DEV_VERSION}" 
```

## X. Add build artefact to the Gitlab release

For now, do this manually.

Later, leverage `release-cli` for automation. See [Issue #518](https://gitlab.com/asqatasun/Asqatasun/-/issues/518)

## X. Update symlink asqatasun-lastest.tar.bz2

On download.Asqatasun.org, update symlink `/asqatasun-lastest.tar.bz2` to point to the actual latest archive
(not the `.war` naither the `.jar`, but the `.tar.bz2` archive)
