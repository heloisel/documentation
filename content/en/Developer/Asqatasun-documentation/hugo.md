---
title: "Hugo"
date: 2020-08-09T11:45:42+02:00
--- 

## Hugo links

* [Hugo documentation](https://gohugo.io/documentation/)
* [Hugo forum](https://discourse.gohugo.io/) (really useful)

## Hugo basics

* All content is written in Markdown, and placed in `content/` directory.
* Configuration of the website is in `config/` directory.
* Once build, the generated website is placed in `static/` directory (important for Gitlab Pages).
* Themes may be used, they are placed in `themes/` directory. Please note they are inserted as git submodules.

## Creating a new page

From the repository's root:

```shell script
hugo new  "Developer/Documentation/doc_guidelines.md"
``` 

You must be at the root of the repo. This prevents shell from auto-completing the path :(
A workaround consists in auto-completing with shell, then removing the beginning of the path, here `content/en/`.

## Hugo integration within IntelliJ

* Plugin [Hugo Integration](https://plugins.jetbrains.com/plugin/13215-hugo-integration)
* Or directly within IntelliJ: `File` > `Settings` > `Plugins`, then search `Hugo`

## Run Hugo locally

From the repository's root:

```shell script
hugo server --forceSyncStatic --watch --i18n-warnings --disableFastRender
```

Then browse [http://localhost:1313/](http://localhost:1313/)
