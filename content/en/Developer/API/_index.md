---
title: "API"
date: 2020-11-21T17:25:06+01:00
---

## Pre-requisites

* **Caution** API usage is meant for developers. This is not an end-user feature, as development knowledge is required.
* Asqtasun v5+ (the REST Server API, not the webapp)

## API Documentation + Swagger

![Asqatasun Swagger screenshot](Asqatasun-API-swagger-Screenshot_2020-11-21.png)

**Note:** We consider we're playing with the default values in the [Asqatasun-Vagrant](https://gitlab.com/asqatasun/asqatasun-vagrant/)
 or [Asqatasun-Docker](https://gitlab.com/asqatasun/asqatasun-docker)

To get to the Swagger user interface, do:

- In your browser: `http://127.0.0.1:8081/`
- Use this user and this password to log in:
    - `admin@asqatasun.org`
    - `myAsqaPassword`

## Getting started with Asqatasun API

### Launch an audit via API

Launch an audit via the API using the following command lines:

```shell
ASQA_USER="admin%40asqatasun.org"
ASQA_PASSWORD="myAsqaPassword"
API_PREFIX_URL="http://${ASQA_USER}:${ASQA_PASSWORD}@localhost:8081"
API_URL="${API_PREFIX_URL}/api/v1/audit/run"

PROJECT_ID="1"
REFERENTIAL="RGAA_4_0"
LEVEL="AA"
URL_TO_AUDIT=https://www.wikidata.org

curl -X POST \
     "${API_URL}"                                               \
     -H  "accept: */*"                                          \
     -H  "Content-Type: application/json"                       \
     -d "{                                                      \
            \"urls\": [    \"${URL_TO_AUDIT}\"  ],              \
                           \"referential\": \"${REFERENTIAL}\", \
                           \"level\": \"${LEVEL}\",             \
                           \"contractId\": ${PROJECT_ID},       \
                           \"tags\": []                         \
         }"
```

### API returns the audit ID

API returns the ID of the audit that has just been launched.

```shell
7 # Audit ID
```

### Display the result of an audit via API

Display the result of an audit via API using the following command lines:

```shell
ASQA_USER="admin%40asqatasun.org"
ASQA_PASSWORD="myAsqaPassword"
API_PREFIX_URL="http://${ASQA_USER}:${ASQA_PASSWORD}@localhost:8081"

AUDIT_ID="7"
API_URL="${API_PREFIX_URL}/api/v1/audit/${AUDIT_ID}"
curl -X GET "${API_URL}" -H  "accept: */*"
```

### API returns JSON data

API returns a JSON content which contains information of requested audit (status, results, ...).

```json
{
   "referential" : "RGAA_4_0",
   "subject" : {
      "grade" : "E",
      "type" : "PAGE",
      "repartitionBySolutionType" : [
         {
            "type" : "PASSED",
            "number" : 8
         },
         {
            "number" : 3,
            "type" : "FAILED"
         },
         {
            "number" : 29,
            "type" : "NEED_MORE_INFO"
         },
         {
            "number" : 71,
            "type" : "NOT_APPLICABLE"
         },
         {
            "type" : "NOT_TESTED",
            "number" : 146
         }
      ],
      "id" : 1,
      "url" : "https://www.wikidata.org/wiki/Wikidata:Main_Page",
      "mark" : 72.73,
      "nbOfPages" : 1
   },
   "id" : 1,
   "status" : "COMPLETED",
   "date" : "2020-11-07T14:08:36.000+0000",
   "referentialLevel" : "LEVEL_2",
   "tags" : []
}
```
