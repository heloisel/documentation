# Documentation

Asqatasun Documentation. Content of https://doc.asqatasun.org/

## Pre-requisites

* [Install Hugo](https://gohugo.io/getting-started/installing/), cross-platform binary is recommended (package from Ubuntu is not, for being too old).

## Compilation

For local development:

```shell script
hugo serve --i18n-warnings --debug -v
```

To generate static pages:

```shell script
hugo
```

All static pages are placed in `public/` directory
